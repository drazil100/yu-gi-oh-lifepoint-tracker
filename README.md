Yi-Gi-Oh Lifepoint Tracker for the TI-83+/TI-84+ Graphing Calculator

Program converted from 8xp to text at https://www.cemetech.net/sc/

Download: https://bitbucket.org/drazil100/yu-gi-oh-lifepoint-tracker/downloads/YUGIOH.8xp
